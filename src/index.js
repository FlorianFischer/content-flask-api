import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import api from './api';
import config from './config.json';
import helmet from 'helmet';
import compression from 'compression';
import passport from 'passport';
import User from './models/user';
import localSignup from './helpers/local-signup';
import localLogin from './helpers/local-login';
import sgMail from '@sendgrid/mail';
import dotenv from 'dotenv';
import globalLog from 'global-request-logger';


let app = express();
app.server = http.createServer(app);

// logger
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'));
  globalLog.initialize();

  globalLog.on('success', function(request, response) {
    console.log('OUTGOING', request.method, request.href, response.statusCode);
  });

  globalLog.on('error', function(request, response) {
    console.log('OUTGOING', request.method, request.href, response.statusCode);
  });
}

// 3rd party middleware
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(bodyParser.json({
  limit : config.bodyLimit
}));

// connect to db
initializeDb().then(db => {
  passport.use('local-signup', localSignup);
  passport.use('local-login', localLogin);
  app.use(passport.initialize());

  // api router
  app.use('/api', api({ config, db }));

  app.use((err, req, res, next) => {
    console.log(err);
    res.status(err.status || 500);
    const error = {
      success: false,
      message: err.message,
    };

    if (err.errors) error.errors = err.errors;
    res.json(error).end();
  });

  app.server.listen(process.env.PORT || config.port, () => {
    console.log(`Started on port ${app.server.address().port}`);
  });
});

export default app;
