import nodemailer from 'nodemailer';
import sgMail from '@sendgrid/mail';
import Q from 'q';


const { SENDGRID_API_KEY } = process.env;


export default (email) => {
  sgMail.setApiKey(SENDGRID_API_KEY);
  return sgMail.send(email)
    .catch(error => {
      return {
        status: 400,
        success: false,
        message: messages['email-send-failure']
      };
    });
};
