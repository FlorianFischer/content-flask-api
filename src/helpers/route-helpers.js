import fs from 'fs';
import path from 'path';

export default {
  recursiveRoutes(folderName, app) {
    fs.readdirSync(folderName).forEach(file => {
      const fullName = path.join(folderName, file);
      const stat = fs.lstatSync(fullName);

      if (stat.isDirectory()) {
        this.recursiveRoutes(fullName, app);
      } else if (!file.toLowerCase().match(/(\.map|index)/)) {
        require(fullName).default(app);
      }
    })
  }
}
