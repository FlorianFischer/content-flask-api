import jwt from 'jsonwebtoken';
import User from '../models/user';
import Company from '../models/company';
import passportLocal from 'passport-local';
import config from '../config';


const LocalStrategy = passportLocal.Strategy;
/**
 * Return the Passport Local Strategy object.
 */
const strategy = new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  session: false,
  passReqToCallback: true
}, (req, email, password, done) => {
  let savedUser;

  const { firstName, lastName, company } = req.body;
  const userData = {
    email: email.trim(),
    password: password.trim(),
    firstName: firstName.trim(),
    lastName: lastName.trim()
  };

  const newUser = new User(userData);
  newUser.save()
    .then(user => {
      savedUser = user;
      const newCompany = new Company({
        name: company.trim(),
      });

      newCompany.users.push(user);
      return newCompany.save();
    })
    .then(company => {
      savedUser.company = company._id;
      return savedUser.save();
    })
    .then(() => {
      const payload = { sub: savedUser._id };
      const secret = process.env.jwtSecret || config.jwtSecret;
      const token = jwt.sign(payload, secret, { expiresIn: 60 * 60 });
      done(null, token, savedUser)
    })
    .catch(done);
});


export default strategy;
