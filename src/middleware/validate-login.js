import validator from 'validator';
import messages from '../lib/messages';


export default (req, res, next) => {
  const { email, password } = req.body;
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (typeof email !== 'string' || email.trim().length === 0) {
    isFormValid = false;
    errors.email = messages['email-missing'];
  }

  if (typeof password !== 'string' || password.trim().length === 0) {
    isFormValid = false;
    errors.password = messages['password-missing'];
  }

  if (!isFormValid) {
    message = messages['form-error']
    return res.status(400).json({
      success: false,
      message,
      errors
    }).end();
  }

  return next();
}
