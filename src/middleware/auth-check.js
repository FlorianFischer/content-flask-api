import jwt from 'jsonwebtoken';
import User from '../models/user';
import config from '../config';
import messages from '../lib/messages';


export default (req, res, next) => {
  const errorObject = {
    message: messages['unauthorized'],
    success: false,
    status: 401
  };

  if (!req.headers.authorization) return res.status(401).json(errorObject).end();

  // get the last part from a authorization header string like "bearer token-value"
  const token = req.headers.authorization.split(' ')[1];
  const secret = process.env.jwtSecret || config.jwtSecret;
  // decode the token using a secret key-phrase
  return jwt.verify(token, secret, (err, decoded) => {
    // the 401 code is for unauthorized status
    if (err) return res.status(401).json(errorObject).end();

    const userId = decoded.sub;

    // check if a user exists
    return User.findById(userId, (userErr, user) => {
      if (userErr || !user) return res.status(401).json(errorObject).end();
      res.locals.user = user;
      res.locals.token = token;

      return next();
    });
  });
};
