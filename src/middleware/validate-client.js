import validator from 'validator';
import messages from '../lib/messages';


export default (req, res, next) => {
  const { email, name, company_name } = req.body;
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (typeof email !== 'string' || !validator.isEmail(email)) {
    isFormValid = false;
    errors.email = messages['email-invalid'];
  }

  if (email.trim().length === 0) {
    isFormValid = false;
    errors.email = messages['email-missing'];
  }

  if (typeof name !== 'string' || name.trim().length === 0) {
    isFormValid = false;
    errors.name = messages['name-missing'];
  }

  if (typeof company_name !== 'string' || name.trim().length === 0) {
    isFormValid = false;
    errors.company_name = messages['company-name-missing'];
  }

  if (Object.keys(req.body).length > 3) {
    isFormValid = false;
  }

  if (!isFormValid) {
    message = messages['form-error']
    return res.status(400).json({
      success: false,
      message,
      errors
    }).end();
  }

  return next();
}
