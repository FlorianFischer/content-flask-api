import validator from 'validator';
import messages from '../lib/messages';


export default (req, res, next) => {
  const {
    email,
    password,
    firstName,
    lastName,
    company
  } = req.body;

  const errors = {};
  let isFormValid = true;
  let message = '';

  if (typeof email !== 'string' || !validator.isEmail(email)) {
    isFormValid = false;
    errors.email = messages['email-invalid'];
  }

  if (typeof password !== 'string' || password.trim().length < 8) {
    isFormValid = false;
    errors.password = messages['password-too-short'];
  }

  if (typeof firstName !== 'string' || firstName.trim().length === 0) {
    isFormValid = false;
    errors.firstName = messages['first-name-missing'];
  }

  if (typeof lastName !== 'string' || lastName.trim().length === 0) {
    isFormValid = false;
    errors.lastName = messages['last-name-missing'];
  }

  if (typeof company !== 'string' || company.trim().length === 0) {
    isFormValid = false;
    errors.company = messages['company-missing'];
  }

  if (!isFormValid) {
    message = messages['form-error']
    return res.status(400).json({
      success: false,
      message,
      errors
    }).end();
  };

  return next();
};
