import mongoose from 'mongoose';
import Q from 'q';

export default () => {
  return Q.Promise((resolve, reject) => {
    const localDb = process.env.NODE_ENV === 'test' ? 'test' : 'admin';
    const uri = process.env.MONGODB_URI || `mongodb://127.0.0.1:27017/${localDb}`;


    mongoose.connect(uri, {
      useMongoClient: true,
      promiseLibrary: Q
    })
    .then(() => {
      console.log('Success: connection to database successful');
      const db = mongoose.connection;
      db.on('error', console.error.bind(console, 'connection error:'));

      resolve(db);
    })
    .catch(() => {
      console.log('Failure: connection to database failed');
      reject();
    });
  });
}
