import mongoose from 'mongoose';
import User from '../models/user';
import Company from '../models/company';
import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index';
import messages from '../lib/messages';
import Q from 'q';
import nock from 'nock';


const assert = chai.assert;
chai.use(chaiHttp);

nock('https://api.sendgrid.com')
  .post('/v3/mail/send')
  .reply(200, {
    message: 'success'
  });


describe('Auth', () => {
  beforeEach(done => {
    Q.all([
      Company.remove({}),
      User.remove({})
    ])
    .then(() => done());
  });


  describe('/POST signup', () => {
    it('Signup validation should return correct errors', done => {
      chai.request(server)
        .post('/api/signup')
        .send({})
        .end((err, res) => {
          const { errors, message } = res.body
          assert.equal(res.status, 400);
          assert.equal(message, messages['form-error']);
          assert.equal(typeof errors, 'object');
          assert.equal(errors.email, messages['email-invalid']);
          assert.equal(errors.password, messages['password-too-short']);
          assert.equal(errors.password, messages['password-too-short']);
          assert.equal(errors.firstName, messages['first-name-missing']);
          assert.equal(errors.lastName, messages['last-name-missing']);
          assert.equal(errors.company, messages['company-missing']);
          done()
        });
    });


    it('Signup should return user and token', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          const { user, token, message, success } = res.body;
          assert.equal(res.status, 200);
          assert.equal(success, true);
          assert.equal(message, messages['signup-successful']);
          assert.equal(typeof user, 'object');
          assert.notExists(user.password);
          assert.equal(typeof user.company, 'object');
          assert.equal(typeof token, 'string');
          done();
        });
    });


    it('Duplicate emails should throw 409', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).post('/api/signup').send({
            firstName: 'Herbert',
            lastName: 'Schulz',
            company: 'Heinz Ketchup Two',
            email: 'herbert@heinz.com',
            password: 'password'
          })
          .end((err, res) => {
            const { errors, message, success } = res.body;
            assert.equal(res.status, 409);
            assert.equal(success, false);
            assert.equal(message, messages['form-error']);
            assert.equal(errors.email, messages['email-taken']);
            done();
          });
        });
    });


    it('Duplicate companies should throw 409', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).post('/api/signup').send({
            firstName: 'Herbert',
            lastName: 'Schulz',
            company: 'Heinz Ketchup',
            email: 'herbert@heinztwo.com',
            password: 'password'
          })
          .end((err, res) => {
            const { errors, message, success } = res.body;
            assert.equal(res.status, 409);
            assert.equal(success, false);
            assert.equal(message, messages['form-error']);
            assert.equal(errors.company, messages['company-taken']);
            done();
          });
        });
    });
  });


  describe('/POST login', () => {
    it('Login validation should return correct errors', done => {
      chai.request(server)
        .post('/api/login')
        .send({})
        .end((err, res) => {
          const { errors, message } = res.body
          assert.equal(res.status, 400);
          assert.equal(message, messages['form-error']);
          assert.equal(typeof errors, 'object');
          assert.equal(errors.email, messages['email-missing']);
          assert.equal(errors.password, messages['password-missing']);
          done()
        });
    });


    it('Login should return user and token', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).post('/api/login').send({
            email: 'herbert@heinz.com',
            password: 'password'
          })
          .end((err, res) => {
            const { user, token, message, success } = res.body;
            assert.equal(res.status, 200);
            assert.equal(success, true);
            assert.equal(message, messages['login-successful']);
            assert.equal(typeof user, 'object');
            assert.notExists(user.password);
            assert.equal(typeof user.company, 'object');
            assert.equal(typeof token, 'string');
            done();
          });
        });
    });


    it('Wrong email should throw error', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).post('/api/login').send({
            email: 'herbert2@heinz.com',
            password: 'password'
          })
          .end((err, res) => {
            const { errors, message, success } = res.body
            assert.equal(res.status, 400);
            assert.equal(success, false);
            assert.equal(message, messages['incorrect-credentials']);
            done();
          });
        });
    });


    it('Wrong password should throw error', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).post('/api/login').send({
            email: 'herbert@heinz.com',
            password: 'lalala'
          })
          .end((err, res) => {
            const { errors, message, success } = res.body
            assert.equal(res.status, 400);
            assert.equal(success, false);
            assert.equal(message, messages['incorrect-credentials']);
            done();
          });
        });
    });
  });


  describe('/GET auth', () => {
    it('Check auth endpoint should return user and token', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).get('/api/auth')
          .set('Authorization', `Bearer ${res.body.token}`)
          .end((err, res) => {
            const { user, token, message, success } = res.body;
            assert.equal(res.status, 200);
            assert.equal(success, true);
            assert.equal(message, messages['user-authenticated']);
            assert.equal(typeof user, 'object');
            assert.notExists(user.password);
            assert.equal(typeof user.company, 'object');
            assert.equal(typeof token, 'string');
            done();
          });
        });
    });


    it('Auth should return 401 without token', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).get('/api/auth')
          .end((err, res) => {
            const { errors, message, success } = res.body
            assert.equal(res.status, 401);
            assert.equal(success, false);
            assert.equal(message, messages['unauthorized']);
            done();
          });
        });
    });


    it('Auth should return 401 with wrong token', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server).get('/api/auth')
          .set('Authorization', 'invalidtoken')
          .end((err, res) => {
            const { errors, message, success } = res.body
            assert.equal(res.status, 401);
            assert.equal(success, false);
            assert.equal(message, messages['unauthorized']);
            done();
          });
        });
    });
  });


  describe('/POST forgot', () => {
    it('Forgot with correct email should return send email', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {
          chai.request(server)
            .post('/api/forgot')
            .send({email: 'herbert@heinz.com'})
            .end((err, res) => {
              const { message, success } = res.body;
              assert.equal(res.status, 200);
              assert.equal(success, true);
              assert.equal(message, messages['recovery-link-sent']);
              done();
            });
        });
    });


    it('Reset without token should return 400', done => {
      chai.request(server)
        .post('/api/signup')
        .send({
          firstName: 'Herbert',
          lastName: 'Schulz',
          company: 'Heinz Ketchup',
          email: 'herbert@heinz.com',
          password: 'password'
        })
        .end((err, res) => {

          chai.request(server)
            .post('/api/forgot')
            .send({ email: 'herbert@heinz.com' })
            .end((err, res) => {

              chai.request(server)
                .post('/api/reset')
                .send({
                  password: 'newpassword',
                })
                .end((err, res) => {
                  const { message, success } = res.body;
                  assert.equal(res.status, 400);
                  assert.equal(success, false);
                  assert.equal(message, messages['reset-token-invalid']);
                  done();
                });
            });
        });
    });
  });
});
