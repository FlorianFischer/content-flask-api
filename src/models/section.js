import mongoose, { Schema } from 'mongoose';


export const SectionSchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  _tempId: {
    type: String
  },
  name: {
    type: String,
    default: 'New container'
  },
  description: {
    type: String
  },
  has_description: {
    type: Boolean,
    default: false,
    required: true
  }
},
{
  timestamps: true
});


export default mongoose.model('Section', SectionSchema);
