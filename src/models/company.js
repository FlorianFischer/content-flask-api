import mongoose, { Schema } from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';

const CompanySchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  name: {
    type: String,
    unique: 'company-unique',
    required: true,
    trim: true
  },
  users: [{ type: Schema.Types.ObjectId, ref: 'User' }]
},
{
  timestamps: true
});


CompanySchema.plugin(beautifyUnique);


export default mongoose.model('Company', CompanySchema);
