import mongoose, { Schema } from 'mongoose';
const deepPopulate = require('mongoose-deep-populate')(mongoose);
import { PageSchema } from './page';
import mongooseDelete from 'mongoose-delete';


const ProjectSchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String,
    trim: true
  },
  last_modified_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company',
    required: true
  },
  state: {
    type: String,
    trim: true,
    required: true
  },
  deadline: {
    type: Date,
    default: Date.now
  },
  client: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  pages: [PageSchema],
  password_protection: {
    type: Boolean,
    default: true
  }
},
{
  timestamps: true
});


ProjectSchema.plugin(deepPopulate);
ProjectSchema.plugin(mongooseDelete, { overrideMethods: true });


export default mongoose.model('Project', ProjectSchema);
