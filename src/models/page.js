import mongoose, { Schema } from 'mongoose';
import mongoose_delete from 'mongoose-delete';
import { SectionSchema } from './section';

export const PageSchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  _tempId: {
    type: String
  },
  name: {
    type: String,
    default: 'New page'
  },
  description: {
    type: String
  },
  has_description: {
    type: Boolean,
    default: false,
    required: true
  },
  sections: [SectionSchema]
},
{
  timestamps: true
});


PageSchema.plugin(mongoose_delete, { deletedAt: true, overrideMethods: 'all' });


export default mongoose.model('Page', PageSchema);
