import mongoose, { Schema } from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';
import bcrypt from 'bcrypt';


const UserSchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  email: {
    type: String,
    unique: 'email-unique',
    required: true,
    trim: true
  },
  password: {
    type: String,
    select: false
  },
  firstName: {
    type: String,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    required: true,
    trim: true
  },
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  resetPasswordToken: {
    type: String,
    select: false
  },
  resetPasswordExpires: {
    type: String,
    select: false
  }
},
{
  timestamps: true
});


UserSchema.plugin(beautifyUnique);


UserSchema.methods.comparePassword = function comparePassword(password, callback) {
  bcrypt.compare(password, this.password, callback);
};


UserSchema.pre('save', function saveHook(next) {
  const user = this;

  // proceed further only if the password is modified or the user is new
  if (!user.isModified('password')) return next();

  return bcrypt.genSalt((saltError, salt) => {
    if (saltError) return next(saltError);

    return bcrypt.hash(user.password, salt, (hashError, hash) => {
      if (hashError) return next(hashError);

      // replace a password string with hash value
      user.password = hash;

      return next();
    });
  });
});


export default mongoose.model('User', UserSchema);
