import mongoose, { Schema } from 'mongoose';
import findOrCreate from 'mongoose-find-or-create';


const ClientSchema = new Schema({
  __v: {
    type: Number,
    select: false
  },
  email: {
    type: String,
    required: true,
    trim: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  company_name: {
    type: String,
    required: true,
    trim: true
  },
  company: Schema.Types.ObjectId,
  projects: [{ type: Schema.Types.ObjectId, ref: 'Project' }]
},
{
  timestamps: true
});


export default mongoose.model('Client', ClientSchema);
