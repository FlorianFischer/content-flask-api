import { Router } from 'express';
import isAuthenticated from '../middleware/auth-check';
import messages from '../lib/messages';
import Project from '../models/project';
import Page from '../models/page';
import moment from 'moment';
import q from 'q';
import _ from 'lodash';
import Section from '../models/section';


const api = Router();


api.get('/projects', isAuthenticated, (req, res, next) => {
  Project.find({ company: res.locals.user.company })
    .then(projects => {
      return res.status(200).json({
        success: true,
        message: 'get-projects-success',
        payload: projects
      });
    })
    .catch(next);
});


api.post('/projects', isAuthenticated, (req, res, next) => {
  const project = new Project({
    created_by: res.locals.user._id,
    deadline: moment().add(14, 'days'),
    name: 'test project',
    last_modified_by: res.locals.user._id,
    company: res.locals.user.company,
    state: 'draft',
    pages: [new Page({
      sections: [new Section()]
    })]
  });

  project.save()
    .then(() => project.deepPopulate('pages', 'pages.sections'))
    .then(() => {
      return res.status(200).json({
        success: true,
        message: 'create-projects-success',
        payload: project
      });
    })
    .catch(next);
});


api.get('/projects/:id', isAuthenticated, (req, res, next) => {
  Project.findById(req.params.id)
    .then(project => {
      if (!project) {
        return next({
          success: false,
          message: 'project-not-found',
          status: 400
        });
      }

      if (!project.company.equals(res.locals.user.company)) {
        return next({
          success: false,
          message: 'forbidden',
          status: 403
        });
      }

      return project.deepPopulate('pages', 'pages.sections');
    })
    .then(project => {
      return res.status(200).json({
        success: true,
        message: 'get-single-project-success',
        payload: project
      });
    })
    .catch(next);
});


api.put('/projects/:id', isAuthenticated, (req, res, next) => {
  Project.findById(req.params.id)
    .then(project => {
      if (!project.company.equals(res.locals.user.company)) {
        next({
          success: false,
          message: 'forbidden',
          status: 403
        });

        return q.reject();
      }

      if (!project) {
        next({
          success: false,
          message: 'project-not-found',
          status: 400
        });

        return q.reject();
      }

      project = Object.assign(project, req.body);
      project.last_modified_by = res.locals.user._id;
      return project.save();
    })
    .then(project => project.deepPopulate('pages', 'pages.sections'))
    .then(project => {
      return res.status(200).json({
        success: true,
        message: 'get-single-project-success',
        payload: project
      });
    })
    .catch(next);
});


api.delete('/projects/:id', isAuthenticated, (req, res, next) => {
  Project.findById(req.params.id)
    .then(project => {
      if (!project.company.equals(res.locals.user.company)) {
        next({
          success: false,
          message: 'forbidden',
          status: 403
        });

        return q.reject();
      }

      if (!project) {
        next({
          success: false,
          message: 'project-not-found',
          status: 400
        });

        return q.reject();
      }

      return project.delete();
    })
    .then(() => {
      return res.status(200).json({
        success: true,
        message: 'delete-project-success'
      });
    })
    .catch(next);
});


export default api;
