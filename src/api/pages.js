import { Router } from 'express';
import isAuthenticated from '../middleware/auth-check';
import messages from '../lib/messages';
import Page from '../models/page';
import Project from '../models/project';
import q from 'q';


const api = Router();


api.post('/projects/:id/pages', isAuthenticated, (req, res, next) => {
  const page = new Page();

  page.save()
    .then(result => {
      return Project.findOne({ _id: req.params.id, company: res.locals.user.company  });
    })
    .then(project => {
      project.pages.push(page._id);
      return project.save();
    })
    .then(() => {
      return res.status(200).json({
        success: true,
        message: 'create-page-success',
        payload: page
      });
    })
    .catch(next);
});


api.delete('/projects/:id/pages/:pid', isAuthenticated, (req, res, next) => {
  let page;

  Project.findOne({ _id: req.params.id, company: res.locals.user.company  })
    .then(project => {
      if (!project) {
        return next({
          success: false,
          message: 'project-not-found',
          status: 400
        });

        q.reject();
      }

      page = project.pages.id(req.params.pid);

      if (!page) {
        return next({
          success: false,
          message: 'page-not-found',
          status: 400
        });

        q.reject();
      }

      page.remove();
      return project.save();
    })
    .then(() => {
      return res.status(200).json({
        success: true,
        message: 'delete-page-success'
      });
    })
    .catch(next);
});


export default api;
