import { Router } from 'express';
import isAuthenticated from '../middleware/auth-check';
import messages from '../lib/messages';
import Client from '../models/client';
import validateClient from '../middleware/validate-client';
import q from 'q';

const api = Router();


api.post('/clients', isAuthenticated, validateClient, (req, res, next) => {
  const { company } = res.locals.user;

  Client.findOne({ email: req.body.email, company })
    .then(client => {
      if (client) {
        next({
          success: false,
          message: 'client-already-exists',
          status: 409
        });

        return q.reject();
      }

      const newClient = new Client({
        ...req.body,
        created_by: res.locals.user.id,
        company
      });

      return newClient.save();
    })
    .then(savedClient => {
      return res.status(200).json({
        success: true,
        message: 'client-create-success',
        payload: savedClient
      });
    })
    .catch(next);
});


api.get('/clients', isAuthenticated, (req, res, next) => {
  Client.find({ company: res.locals.user.company })
    .then(clients => {
      clients = clients || [];

      return res.status(200).json({
        success: true,
        message: 'get-clients-success',
        payload: clients
      });
    })
    .catch(next);
});


export default api;
