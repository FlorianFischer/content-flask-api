import { version } from '../../package.json';
import { Router } from 'express';
import helpers from '../helpers/route-helpers';
import authRoutes from './auth';
import projectRoutes from './projects';
import clientRoutes from './clients';
import pageRoutes from './pages';


export default ({ config, db }) => {
  const api = Router();

  // perhaps expose some API metadata at the root
  api.get('/', (req, res) => {
    res.json({ version });
  });

  api.use(authRoutes);
  api.use(projectRoutes);
  api.use(clientRoutes);
  api.use(pageRoutes);

  // recursively import all routes from this directory
  // helpers.recursiveRoutes(__dirname, { api, db, config });

  return api;
}
