import User from '../models/user';
import passport from 'passport';
import passportLocal from 'passport-local';
import validator from 'validator';
import isAuthenticated from '../middleware/auth-check';
import crypto from 'crypto';
import q from 'q';
import sendMail from '../helpers/send-mail';
import { Router } from 'express';
import config from '../config';
import messages from '../lib/messages';
import validateSignup from '../middleware/validate-signup';
import validateLogin from '../middleware/validate-login';


const promisedRandBytes = q.denodeify(crypto.randomBytes);
const api = Router();


api.post('/signup', validateSignup, (req, res, next) => {
  return passport.authenticate('local-signup', (err, token, user) => {
    if (err) {
      if (err.name === 'ValidationError') {
        const  errorObject = {
          status: 409,
          message: messages['form-error'],
          errors: {}
        };

        if (err.errors.name) errorObject.errors.company = messages['company-taken'];
        if (err.errors.email) errorObject.errors.email = messages['email-taken'];

        return next(errorObject);
      }

      return next({
        message: 'general-error',
        status: 400
      });
    }

    user.populate('company').execPopulate()
      .then(populatedUser => {
        delete populatedUser._doc.password;

        return res.status(200).json({
          success: true,
          message: messages['signup-successful'],
          payload: {
            user: populatedUser,
            token
          }
        });
      })
      .catch(error => {
        return next({
          message: 'general-error',
          status: 400
        });
      });
  })(req, res, next);
});


api.post('/login', validateLogin, (req, res, next) => {
  return passport.authenticate('local-login', (err, token, user) => {
    if (err) {
      if (err.name === 'IncorrectCredentialsError') {
        return next({
          message: messages['incorrect-credentials'],
          status: 400
        });
      }

      return next({
        message: messages['general-error'],
        status: 400
      });
    }

    user.populate('company').execPopulate()
      .then(populatedUser => {
        delete populatedUser._doc.password;

        return res.status(200).json({
          success: true,
          message: messages['login-successful'],
          payload: {
            user: populatedUser,
            token
          }
        });
      })
      .catch(error => {
        return next({
          message: messages['general-error'],
          status: 400
        });
      });
  })(req, res, next);
});


api.post('/forgot', (req, res, next) => {
  let token;

  promisedRandBytes(20)
    .then(buffer => token = buffer.toString('hex'))
    .then(() => User.findOne({ email: req.body.email }).exec())
    .then(user => {
      if (!user) {
        res.json({
          success: true,
          message: messages['recovery-link-sent']
        });

        return q.reject();
      }

      user.resetPasswordToken = token;
      user.resetPasswordExpires = Date.now() + 3600000;

      return user.save();
    })
    .then(user => {
      const email = {
        from: config.fromEmail,
        to: user.email,
        subject: 'Password reset link',
        html: `<a href="${process.env.CLIENT_URI}/reset/${token}">Click this link to reset your password</a>`
      };

      return sendMail(email);
    })
    .then(() => {
      return res.json({
        success: true,
        message: messages['recovery-link-sent']
      });
    })
    .catch(next);
});


api.get('/auth', isAuthenticated, (req, res, next) => {
  const { user, token } = res.locals;

  user.populate('company').execPopulate()
    .then(populatedUser => {
      return res.status(200).json({
        success: true,
        message: messages['user-authenticated'],
        payload: { user: populatedUser, token }
      });
    })
    .catch(error => {
      return next({
        message: messages['general-error'],
        status: 400
      });
    });
});


api.post('/reset', (req, res, next) => {
  const { token, password } = req.body;

  if (!token) {
    return next({
      success: false,
      status: 400,
      message: messages['reset-token-invalid']
    });
  } else if (!password || password.length < 8) {
    return next({
      success: false,
      status: 400,
      message: messages['password-missing']
    });
  }

  User.findOne({ resetPasswordToken: token })
    .select('+resetPasswordExpires')
    .then(user => {
      if (user.resetPasswordExpires < Date.now() ||!user) {
        return next({
          success: false,
          status: 400,
          message: messages['reset-token-invalid']
        });
      }

      user.resetPasswordToken = undefined;
      user.resetPasswordExpires = undefined;
      user.password = password;
      return user.save();
    })
    .then(user => {
      const email = {
        from: config.fromEmail,
        to: user.email,
        subject: 'Password reset sucessful',
        html: `Your password has beed successfully reset. <a href="${process.env.CLIENT_URI}/login">Login</a>`
      };

      return sendMail(email);
    })
    .then(() => {
      return res.status(200).json({
        success: true,
        message: messages['password-reset-successful']
      });
    })
    .catch(error => {
      return next({
        success: false,
        status: 400,
        message: 'reset-token-invalid'
      });
    });
});


export default api;
